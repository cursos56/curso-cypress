/// <reference types="cypress" />

describe('Tests to be Page', () => {

    const _url = 'http://4alltests.com.br/testeapis/';
    it('Test page basic visit and test title page', () => {
        cy.visit(_url);

        cy.title()
            .should('equal', 'Teste sua api...')
            .and('contain', '...');
    });

    it('Test page interative element', () => {
        cy.visit(_url);

        cy.get('#addauthbutton').click();
        cy.get('#authentication > :nth-child(1) > .span2 > .control-group > .control-label')
        .should('contain', 'Username');
    });
});
