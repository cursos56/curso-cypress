/// <reference types="cypress" />

describe('Tests to be sync text page', () => {
    before(() => {
        const _url = 'https://www.wcaquino.me/cypress/componentes.html';
        cy.visit(_url);
    });

    beforeEach(() => {
        cy.reload();
    });

    it('Test sync element insert value', () => {
        const _newValue = 'Funcionou!';

        cy.get('#novoCampo').should('not.exist');
        cy.get('#buttonDelay').click();
        cy.get('#novoCampo').should('not.exist');
        cy.get('#novoCampo').should('exist');
        cy.get('#novoCampo').type(_newValue);
    });

    it('Test sync element insert value retrys', () => {
        const _newValue = 'Funcionou!';

        cy.get('#buttonDelay').click();
        cy.get('#novoCampo').should('not.exist');
        cy.get('#novoCampo').should('exist').type(_newValue);
    });

    it.only('Test sync list find element', () => {
        const _findItem = 'Item 1';
        const _findItem2 = 'Item 2';

        cy.get('#buttonListDOM').click();
        cy.get('#lista > :nth-child(1)').find('span').should('have.text', _findItem); // find só funciona pro primeiro elemento pesquisado
        cy.get('#lista > :nth-child(2) span').should('have.text', _findItem2);
    });
});