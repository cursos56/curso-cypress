/// <reference types="cypress" />

describe('Tests to be Equality', () => {
    const a = 1;

    it('Test equality basic', () => {
        expect(a).equal(1);
    });

    it('Test equality basic in describe ', () => {
        expect(a, 'Test a equal const number').equal(1);
    });

    it('Test equality advanced information code', () => {
        expect(a, 'Test a equal const number').to.be.equal(1);
    });

    it('Test equality advanced information code not equal', () => {
        expect(a, 'Test a not equal const number').not.to.be.equal(2);
    });
});

describe('Tests to be Truthy', () => {
    const a = true;
    const b = null;
    let c;

    it('Test truthy basic', () => {
        expect(a).equal(true);
    });

    it('Test truthy basic in decribe code', () => {
        expect(a).to.be.equal(true);
    });
    
    it('Test truthy diference null', () => {
        expect(a).to.be.not.null;
    });

    it('Test truthy equal null', () => {
        expect(b).to.be.null;
    });

    it('Test truthy equal undefined', () => {
        expect(c).to.be.undefined;
    });
});

describe('Tests to be Object', () => {
    const obj = {
        a: 1,
        b: 2
    };

    it('Test object basic', () => {
        expect(obj).equal(obj); 
    });

    it('Test object resume', () => {
        expect(obj).eq(obj); 
    });

    it('Test object estruct long code', () => {
        expect(obj).to.be.deep.equal(obj); 
    });

    it('Test object estruct resume code', () => {
        expect(obj).eql(obj); 
    });

    it('Test object search item', () => {
        expect(obj).include({ a: 1 }); 
    });

    it('Test object search property', () => {
        expect(obj).to.have.property('b'); 
    });

    it('Test object search not property', () => {
        expect(obj).to.not.have.property('c'); 
    });

    it('Test object search item in property', () => {
        expect(obj).to.have.property('b', 2); 
    });

    it('Test object not empty', () => {
        expect(obj).to.not.empty; 
    });
});


describe('Tests to be Array', () => {
    const array = [1,2,3];

    it('Test array basic', () => {
        expect(array).equal(array); 
    });

    it('Test array estruct long code', () => {
        expect(array).to.be.deep.equal([1,2,3]); 
    });
    
    it('Test array estruct resume code', () => {
        expect(array).eql([1,2,3]); 
    });

    it('Test array search item', () => {
        expect(array).include(1); 
    });

    it('Test array not empty', () => {
        expect(array).to.not.empty; 
    });
});


describe('Tests to be Number', () => {
    const number = 1;
    const floatNumber = 5.234;

    it('Test number basic', () => {
        expect(number).equal(1); 
    });

    it('Test float number basic', () => {
        expect(floatNumber).equal(5.234); 
    });

    it('Test number above', () => {
        expect(number).to.be.above(0); 
    });

    it('Test float number below', () => {
        expect(floatNumber).to.be.below(6); 
    });
});