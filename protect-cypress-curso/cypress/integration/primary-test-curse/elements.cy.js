/// <reference types="cypress" />

describe('Tests to be elemente text page', () => {
    before(() => {
        const _url = 'https://www.wcaquino.me/cypress/componentes.html';
        cy.visit(_url);
    });

    beforeEach(() => {
        cy.reload();
    });

    it('Test get text element', () => {
        cy.get('#buttonSimple')
            .click()
            .should('have.value', 'Obrigado!');
    });

    it('Test get link element', () => {
        cy.get('[href="#"]').click();
        cy.get('#resultado').should('have.text', 'Voltou!');
    });

    it('Test set new value in element', () => {
        const _newNome = 'Lênin';
        const _sobreNomeErro = 'M Samp';
        const _sobreNome = 'Müller Sampaio';

        cy.get('#formNome').type(_newNome);
        cy.get('[data-cy="dataSobrenome"]').type(_sobreNomeErro).clear().type(_sobreNome);
    });

    it('Test select combobox', () => {
        const _selectValue = '2graucomp';

        cy.get('[data-test="dataEscolaridade"]').select(_selectValue).should('have.value', _selectValue);
    });

    it('Test select multiple combobox', () => {
        const _selectValues = ['Corrida', 'natacao', 'nada'];

        cy.get('[data-testid="dataEsportes"]').select(_selectValues);
    });
});